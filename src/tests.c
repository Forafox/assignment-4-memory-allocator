#define _GNU_SOURCE
#include "mem.h"
#include "mem_internals.h"
#include "tests.h"
#include "util.h"
#include <stddef.h>
#include <stdio.h>

#define SPLITTER_LENGTH 30
#define HEAP_SIZE 80000
#define BLOCK1_SIZE 500
#define BLOCK2_SIZE 1000
#define BLOCK3_SIZE 1500
#define BLOCK4_SIZE 2000
#define BLOCK5_SIZE 700
#define BLOCK6_SIZE 2000
#define LARGE_BLOCK_SIZE 50000
#define NEW_REGION_SIZE 1024
#define NEW_BLOCK_SIZE 100000


static struct block_header* get_block_by_address(void* data) {
    size_t offset = offsetof(struct block_header, contents);
    uint8_t* block_start = (uint8_t*)data - offset;
    return (struct block_header*)block_start;
}

void* map_pages(void const* addr, size_t length) {
    return mmap((void*)addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE, -1, 0);
}

static void print_splitter(int length) {
    for (int i = 0; i < length; ++i) {
        printf("-");
    }
    printf("\n");
}

void print_test_description(int test_number, const char* description) {
    printf("Test %d: %s\n", test_number, description);
}

void print_test_result(int test_number, const char* result) {
    printf("Test %d %s\n\n", test_number, result);
}

void free_and_check_all(const char* error_message, int count, void** data) {
    for (int i = 0; i < count; ++i) {
        void* current_data = data[i];
        if (current_data == NULL) {
            err(error_message);
        } else {
            _free(current_data);
        }
    }
}

void test_normal_allocation(struct block_header* heap, int* success_count) {
    print_test_description(1, "Successful Memory Allocation");

    const size_t size = BLOCK1_SIZE;
    void* data = _malloc(size);

    if (data == NULL) {
        err("Error: malloc failed to allocate memory\n");
    }

    debug_heap(stdout, heap);

    if (heap->is_free) {
        err("Error: Allocated block is free\n");
    }

    if (heap->capacity.bytes != size) {
        err("Error: Incorrect capacity value\n");
    }

    print_test_result(1, "passed successfully!");
    (*success_count)++;

    void* data_array[] = { data };
    free_and_check_all("Error: failed to free memory", 1, data_array);
}

void test_free_one_of_multiple_blocks(struct block_header* heap, int* success_count) {
    print_test_description(2, "Freeing One Block out of Multiple Allocations");

    const int num_allocations = 2;
    void* data[num_allocations];

    data[0] = _malloc(BLOCK1_SIZE);
    data[1] = _malloc(BLOCK5_SIZE);

    for (int i = 0; i < num_allocations; ++i) {
        if (data[i] == NULL) {
            err("Error: malloc failed to allocate memory\n");
            return;
        }
    }

    _free(data[0]);

    debug_heap(stdout, heap);

    struct block_header* block1 = get_block_by_address(data[0]);
    struct block_header* block2 = get_block_by_address(data[1]);

    if (!block1->is_free) {
        err("Error: Free block is already occupied\n");
    }

    if (block2->is_free) {
        err("Error: Allocated block is free\n");
    }

    print_test_result(2, "passed successfully!");
    (*success_count)++;

    void* data_array[] = { data[0], data[1] };
    free_and_check_all("Error: failed to free memory", 2, data_array);
}

void test_free_two_blocks_out_of_multiple_allocations(struct block_header* heap, int* success_count) {
    print_test_description(3, "Freeing Two Blocks out of Multiple Allocations");

    const int num_allocations = 3;
    void* data[num_allocations];

    data[0] = _malloc(BLOCK1_SIZE);
    data[1] = _malloc(BLOCK2_SIZE);
    data[2] = _malloc(BLOCK3_SIZE);

    for (int i = 0; i < num_allocations; ++i) {
        if (data[i] == NULL) {
            err("Error: malloc failed to allocate memory\n");
            return;
        }
    }

    _free(data[1]);
    _free(data[0]);

    debug_heap(stdout, heap);

    struct block_header* block1 = get_block_by_address(data[0]);
    struct block_header* block3 = get_block_by_address(data[2]);

    if (!block1->is_free) {
        err("Error: Block is already occupied\n");
    }

    if (block3->is_free) {
        err("Error: Block is free\n");
    }

    if (block1->capacity.bytes != BLOCK1_SIZE + BLOCK2_SIZE + offsetof(struct block_header, contents)) {
        err("Error: Free blocks are not coalesced\n");
    }

    print_test_result(3, "passed successfully!");
    (*success_count)++;

    void* data_array[] = { data[0], data[1], data[2] };
    free_and_check_all("Error: failed to free memory", num_allocations, data_array);
}

void test_memory_exhaustion_and_expansion(struct block_header* heap, int* success_count) {
    print_test_description(4, "Memory Exhaustion and Expansion");

    const int num_allocations = 3;
    void* data[num_allocations];

    data[0] = _malloc(HEAP_SIZE);
    data[1] = _malloc(HEAP_SIZE + BLOCK2_SIZE);
    data[2] = _malloc(BLOCK6_SIZE);

    for (int i = 0; i < num_allocations; ++i) {
        if (data[i] == NULL) {
            err("Error: malloc failed to allocate memory\n");
            return;
        }
    }

    _free(data[2]);
    _free(data[1]);

    debug_heap(stdout, heap);

    struct block_header* block1 = get_block_by_address(data[0]);
    struct block_header* block2 = get_block_by_address(data[1]);

    if ((uint8_t*)block1->contents + block1->capacity.bytes != (uint8_t*)block2) {
        err("Error: New region was not created after the old one\n");
    }

    print_test_result(4, "passed successfully!");
    (*success_count)++;

    void* data_array[] = { data[0], data[1], data[2] };
    free_and_check_all("Error: failed to free memory", num_allocations, data_array);
}

void test_memory_exhaustion_and_new_region_allocation(struct block_header* heap, int* success_count) {
    print_test_description(5, "Memory Exhaustion, No Expansion due to another allocated range, New Region Allocated Elsewhere");

    const int num_allocations = 2;
    void* data[num_allocations];

    data[0] = _malloc(LARGE_BLOCK_SIZE);
    if (data[0] == NULL) {
        err("Error: malloc failed to allocate memory\n");
        return;
    }

    struct block_header* address = heap;
    while (address->next != NULL) {
        address = address->next;
    }

    map_pages((uint8_t*)address + size_from_capacity(address->capacity).bytes, NEW_REGION_SIZE);

    data[1] = _malloc(NEW_BLOCK_SIZE);

    for (int i = 0; i < num_allocations; ++i) {
        if (data[i] == NULL) {
            err("Error: malloc failed to allocate memory\n");
            return;
        }
    }

    debug_heap(stdout, heap);

    struct block_header* block2 = get_block_by_address(data[1]);

    if (block2 == address) {
        err("Error: Block was not allocated in a new location\n");
    }

    print_test_result(5, "passed successfully!");
    (*success_count)++;

    void* data_array[] = { data[0], data[1] };
    free_and_check_all("Error: failed to free memory", num_allocations, data_array);
}

void run_all_tests() {
    printf("Heap initialization started\n");

    struct block_header* heap = (struct block_header*)heap_init(HEAP_SIZE);

    if (heap == NULL) {
        err("Heap initialization failed\n");
    }

    int success_count = 0;

    test_normal_allocation(heap, &success_count);
    print_splitter(SPLITTER_LENGTH);
    test_free_one_of_multiple_blocks(heap, &success_count);
    print_splitter(SPLITTER_LENGTH);
    test_free_two_blocks_out_of_multiple_allocations(heap, &success_count);
    print_splitter(SPLITTER_LENGTH);
    test_memory_exhaustion_and_expansion(heap, &success_count);
    print_splitter(SPLITTER_LENGTH);
    test_memory_exhaustion_and_new_region_allocation(heap, &success_count);

    printf("All tests completed. %d out of 5 tests passed successfully!\n", success_count);
}
